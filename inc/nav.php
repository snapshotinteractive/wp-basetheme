<?php
/**
 * Navigation related functions.
 *
 * @package basetheme
 */

if ( ! function_exists( 'ssi_navigation_menus' ) ) {
	/**
	 * Load navigation menus for our theme.
	 */
	function ssi_navigation_menus() {

		$locations = array(
			'main-navigation'      => __( 'Main Navigation', 'basetheme' ),
			'secondary-navigation' => __( 'Secondary Navigation', 'basetheme' ),
			'footer-navigation'    => __( 'Footer Navigation', 'basetheme' ),
			'social'               => __( 'Social Navigation', 'basetheme' ),
		);
		register_nav_menus( $locations );
	}
}

add_action( 'init', 'ssi_navigation_menus' );

if ( ! function_exists( 'ssi_mobile_nav' ) ) {
	/**
	 * Function to generate mobile nav menu.
	 *
	 * @param string $location Registered nav menu location.
	 */
	function ssi_mobile_nav( $location = 'main-navigation' ) {
		if ( has_nav_menu( $location ) ) {
			wp_nav_menu(
				array(
					'container'      => false, // Remove nav container.
					'menu'           => __( 'mobile-nav', 'basetheme' ),
					'menu_class'     => 'vertical menu',
					'theme_location' => esc_attr( $location ),
					'items_wrap'     => '<ul id="%1$s" class="%2$s" data-accordion-menu data-submenu-toggle="true">%3$s</ul>',
					'fallback_cb'    => false,
					'walker'         => new \Basetheme\Foundation\Walker_Nav_Menu_Mobile(),
				)
			);
		}
	}
}
