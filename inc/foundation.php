<?php
/**
 * Make WordPress more Foundation friendly.
 *
 * @package basetheme
 */

if ( ! function_exists( 'ssi_active_nav_class' ) ) :
	/**
	 * Add Foundation 'is-active' class for the current menu item.
	 *
	 * @param  array  $classes CSS classes.
	 * @param  object $item    Nav menu item data object.
	 * @return array           CSS classes.
	 */
	function ssi_active_nav_class( $classes, $item ) {
		if ( $item->current === 1 || $item->current_item_ancestor === true ) {
			$classes[] = 'is-active';
		}
		return $classes;
	}
	add_filter( 'nav_menu_css_class', 'ssi_active_nav_class', 10, 2 );
endif;

if ( ! function_exists( 'ssi_active_list_pages_class' ) ) :
	/**
	 * Use the is-active class of ZURB Foundation on wp_list_pages output.
	 * From required+ Foundation http://themes.required.ch.
	 *
	 * @param  string $input Input string.
	 * @return string        Output string.
	 */
	function ssi_active_list_pages_class( $input ) {
		$pattern = '/current_page_item/';
		$replace = 'current_page_item is-active';
		$output  = preg_replace( $pattern, $replace, $input );
		return $output;
	}
	add_filter( 'wp_list_pages', 'ssi_active_list_pages_class', 10, 2 );
endif;

if ( ! function_exists( 'ssi_responsive_video_oembed_html' ) ) :
	/**
	 * Enable Foundation responsive embeds for WP video embeds
	 *
	 * @param  mixed  $html    The cached HTML result, stored in post meta.
	 * @param  string $url     Video URL.
	 * @param  array  $attr    An array of shortcode attributes.
	 * @param  int    $post_id Post ID.
	 * @return mixed           Modified HTML output.
	 */
	function ssi_responsive_video_oembed_html( $html, $url, $attr, $post_id ) {
		/**
		 * Whitelist of oEmbed compatible sites that **ONLY** support video.
		 * Cannot determine if embed is a video or not from sites that
		 * support multiple embed types such as Facebook.
		 *
		 * Official list can be found here https://codex.wordpress.org/Embeds
		 */
		$video_sites = [
			'youtube', // First for performance.
			'collegehumor',
			'dailymotion',
			'funnyordie',
			'ted',
			'videopress',
			'vimeo',
		];

		$is_video = false;

		// Determine if embed is a video.
		foreach ( $video_sites as $site ) {
			// Match on `$html` instead of `$url` because of shortened URLs like `youtu.be` will be missed.
			if ( strpos( $html, $site ) ) {
				$is_video = true;
				break;
			}
		}

		// Process video embed.
		if ( true === $is_video ) {
			// Find the `<iframe>`.
			$doc = new DOMDocument();
			$doc->loadHTML( $html );
			$tags = $doc->getElementsByTagName( 'iframe' );
			// Get width and height attributes.
			foreach ( $tags as $tag ) {
				$width  = $tag->getAttribute( 'width' );
				$height = $tag->getAttribute( 'height' );
				break; // Should only be one.
			}
			$class = 'responsive-embed'; // Foundation class.

			// Determine if aspect ratio is 16:9 or wider.
			if ( is_numeric( $width ) && is_numeric( $height ) && ( $width / $height >= 1.7 ) ) {
				$class .= ' widescreen'; // Space needed.
			}

			// Wrap oEmbed markup in Foundation responsive embed.
			return '<div class="' . $class . '">' . $html . '</div>';
		} else {
			// If not a supported embed.
			return $html;
		}
	}
	add_filter( 'embed_oembed_html', 'ssi_responsive_video_oembed_html', 10, 4 );
endif;

if ( ! function_exists( 'ssi_sticky_posts' ) ) :
	/**
	 * Change the class for sticky posts to .wp-sticky
	 * to avoid conflicts with Foundation's Sticky plugin
	 *
	 * @param  array $classes CSS classes.
	 * @return array          CSS classes.
	 */
	function ssi_sticky_posts( $classes ) {
		if ( in_array( 'sticky', $classes, true ) ) {
			$classes   = array_diff( $classes, array( 'sticky' ) );
			$classes[] = 'wp-sticky';
		}
		return $classes;
	}
	add_filter( 'post_class', 'ssi_sticky_posts' );
endif;
