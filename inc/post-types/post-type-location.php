<?php
/**
 * Location post type.
 *
 * @package basetheme
 */

if ( ! function_exists( 'ssi_post_type_location' ) ) :
	/**
	 * Register Location post type.
	 */
	function ssi_post_type_location() {

		$labels = [
			'name'               => _x( 'Locations', 'post type general name', 'basetheme' ),
			'singular_name'      => _x( 'Location', 'post type singular name', 'basetheme' ),
			'menu_name'          => _x( 'Locations', 'admin menu', 'basetheme' ),
			'name_admin_bar'     => _x( 'Location', 'add new on admin bar', 'basetheme' ),
			'add_new'            => _x( 'Add New', 'location', 'basetheme' ),
			'add_new_item'       => __( 'Add New Location', 'basetheme' ),
			'new_item'           => __( 'New Location', 'basetheme' ),
			'edit_item'          => __( 'Edit Location', 'basetheme' ),
			'view_item'          => __( 'View Location', 'basetheme' ),
			'all_items'          => __( 'All Locations', 'basetheme' ),
			'search_items'       => __( 'Search Locations', 'basetheme' ),
			'parent_item_colon'  => __( 'Parent Locations:', 'basetheme' ),
			'not_found'          => __( 'No Locations found.', 'basetheme' ),
			'not_found_in_trash' => __( 'No Locations found in Trash.', 'basetheme' ),
		];

		$args = [
			'labels'             => $labels,
			'description'        => __( 'Description.', 'basetheme' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'rewrite'            => [
				'slug'       => 'locations',
				'with_front' => false,
			],
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => [ 'title', 'thumbnail' ],
			'menu_icon'          => 'dashicons-location',
		];

		register_post_type( 'location', $args );
	}
	add_action( 'init', 'ssi_post_type_location' );
endif;
