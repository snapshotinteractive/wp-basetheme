<?php
/**
 * Team post type.
 *
 * @package basetheme
 */

if ( ! function_exists( 'ssi_post_type_team' ) ) :
	/**
	 * Register Team post type.
	 */
	function ssi_post_type_team() {
		$labels = [
			'name'               => _x( 'Team Members', 'post type general name', 'basetheme' ),
			'singular_name'      => _x( 'Team Member', 'post type singular name', 'basetheme' ),
			'menu_name'          => _x( 'Team', 'admin menu', 'basetheme' ),
			'name_admin_bar'     => _x( 'Team Member', 'add new on admin bar', 'basetheme' ),
			'add_new'            => _x( 'Add New', 'location', 'basetheme' ),
			'add_new_item'       => __( 'Add New Team Member', 'basetheme' ),
			'new_item'           => __( 'New Team Member', 'basetheme' ),
			'edit_item'          => __( 'Edit Team Member', 'basetheme' ),
			'view_item'          => __( 'View Team Member', 'basetheme' ),
			'all_items'          => __( 'All Team Members', 'basetheme' ),
			'search_items'       => __( 'Search Team Members', 'basetheme' ),
			'parent_item_colon'  => __( 'Parent Team Members:', 'basetheme' ),
			'not_found'          => __( 'No Team Members found.', 'basetheme' ),
			'not_found_in_trash' => __( 'No Team Members found in Trash.', 'basetheme' ),
		];

		$args = [
			'labels'             => $labels,
			'description'        => __( 'Description.', 'basetheme' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'rewrite'            => [
				'slug'       => 'team',
				'with_front' => false,
			],
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => [ 'title', 'editor', 'thumbnail' ],
			'menu_icon'          => 'dashicons-groups',
		];

		register_post_type( 'team', $args );
	}
	add_action( 'init', 'ssi_post_type_team' );
endif;
