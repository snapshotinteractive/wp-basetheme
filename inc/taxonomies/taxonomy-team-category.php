<?php
/**
 * Team category taxonomy.
 *
 * @package basetheme
 */

if ( ! function_exists( 'ssi_tax_team_category' ) ) :
	/**
	 * Register Team Category taxonomy.
	 */
	function ssi_tax_team_category() {

		$labels = [
			'name'              => _x( 'Team Categories', 'taxonomy general name', 'basetheme' ),
			'singular_name'     => _x( 'Team Category', 'taxonomy singular name', 'basetheme' ),
			'search_items'      => __( 'Search Team Categories', 'basetheme' ),
			'all_items'         => __( 'All Team Categories', 'basetheme' ),
			'parent_item'       => __( 'Parent Team Category', 'basetheme' ),
			'parent_item_colon' => __( 'Parent Team Category:', 'basetheme' ),
			'edit_item'         => __( 'Edit Team Category', 'basetheme' ),
			'update_item'       => __( 'Update Team Category', 'basetheme' ),
			'add_new_item'      => __( 'Add New Team Category', 'basetheme' ),
			'new_item_name'     => __( 'New Team Category Name', 'basetheme' ),
			'menu_name'         => __( 'Team Categories', 'basetheme' ),
		];

		$args = [
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => [
				'slug'         => 'team-categories',
				'hierarchical' => true,
			],
		];

		register_taxonomy( 'team-category', [ 'team' ], $args );
	}
	add_action( 'init', 'ssi_tax_team_category', 0 );
endif;
