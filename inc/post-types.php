<?php
/**
 * Load and handle all post types and taxonomies.
 *
 * @package basetheme
 */

// Define directories for post types and taxonomies.
$ssi_cpt_directories = array(
	'post-type' => get_template_directory() . '/inc/post-types/',
	'taxonomy'  => get_template_directory() . '/inc/taxonomies/',
);

foreach ( $ssi_cpt_directories as $ssi_key => $directory ) {

	foreach ( new DirectoryIterator( $directory ) as $fileinfo ) {
		if ( ! $fileinfo->isDot() && $fileinfo->getExtension() === 'php' ) {

			// Check filename for prefix before loading.
			if ( substr( $fileinfo->getBasename( '.php' ), 0, strlen( $ssi_key ) + 1 ) === $ssi_key . '-' ) {
				require_once $directory . $fileinfo->getFilename();
			}
		}
	}
}
