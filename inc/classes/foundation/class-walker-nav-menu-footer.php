<?php
/**
 * Custom navigation walker for the footer menu.
 *
 * @package basetheme
 */

namespace Basetheme\Foundation;

use       Basetheme\Foundation;

if ( ! defined( 'WPINC' ) ) {
	exit;
}

/**
 * [Walker_Nav_Menu_Footer description]
 */
class Walker_Nav_Menu_Footer extends \Walker_Nav_Menu {

	/**
	 * Begin a new level in the tree..
	 *
	 * @param  string   $output HTML output.
	 * @param  integer  $depth  Depth of menu item. Used for padding.
	 * @param  stdClass $args   An object of wp_nav_menu() arguments.
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ) {

		// Set class strings.
		$footer_class = 'site-footer';
		$base_class   = 'site-nav';
		$base_class_2 = 'footer-nav';

		if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
			$t = '';
			$n = '';
		} else {
			$t = "\t";
			$n = "\n";
		}
		$indent = str_repeat( $t, $depth );

		// Default classes.
		$classes = array(
			"{$footer_class}__menu",
			"{$footer_class}__menu--submenu",
			"{$base_class}",
			"{$base_class}--footer",
			"{$base_class}--submenu",
			"{$base_class_2}",
			"{$base_class_2}--submenu",
		);

		/**
		 * Filters the CSS class(es) applied to a menu list element.
		 *
		 * @since 4.8.0
		 *
		 * @param array    $classes The CSS classes that are applied to the menu `<ul>` element.
		 * @param stdClass $args    An object of `wp_nav_menu()` arguments.
		 * @param int      $depth   Depth of menu item. Used for padding.
		 */
		$class_names = join( ' ', apply_filters( 'nav_menu_submenu_css_class', $classes, $args, $depth ) ); // WPCS: prefix ok.
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

		$output .= "{$n}{$indent}{$t}<ul{$class_names}>{$n}";
	}

	/**
	 * Begin a new menu element.
	 *
	 * @param  string   $output HTML output.
	 * @param  WP_Post  $item   Menu item data object.
	 * @param  integer  $depth  Depth of menu item. Used for padding.
	 * @param  stdClass $args   An object of wp_nav_menu() arguments.
	 * @param  integer  $id     Menu ID.
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
			$t = '';
			$n = '';
		} else {
			$t = "\t";
			$n = "\n";
		}
		$indent = ( $depth ) ? str_repeat( $t, $depth ) : '';

		/**
		 * Filters the arguments for a single nav menu item.
		 *
		 * @param stdClass $args  An object of wp_nav_menu() arguments.
		 * @param WP_Post  $item  Menu item data object.
		 * @param int      $depth Depth of menu item. Used for padding.
		 */
		$args = apply_filters( 'nav_menu_item_args', $args, $item, $depth ); // WPCS: prefix ok.

		// Set class strings.
		$footer_class = 'site-footer';
		$base_class   = 'site-nav';
		$base_class_2 = 'footer-nav';
		$item_el      = 'item';
		$link_el      = 'link';
		$child_mod    = 'child';
		$parent_mod   = 'has-children';
		$col_class    = 'cell';

		if ( ! isset( $this->current_menu ) ) {
			$this->current_menu = wp_get_nav_menu_object( $args->menu );
		}
		/**
		 * Set indent for sub-menu items.
		 *
		 * @var string
		 */
		$indent = ( $depth > 1 ) ? str_repeat( "\t", $depth - 1 ) : '';

		/**
		 * Set class values for menu items.
		 *
		 * @var array
		 */
		$classes = empty( $item->classes ) ? [] : (array) $item->classes;
		$classes = array_merge( $classes, array(
			"{$footer_class}__menu-{$item_el}",
			"{$base_class}__{$item_el}",
			"{$base_class}__{$item_el}--{$item->ID}",
			"{$base_class}__{$item_el}--footer",
			"{$base_class_2}__{$item_el}",
			"{$base_class_2}__{$item_el}--{$item->ID}",
		) );

		if ( '0' !== $item->menu_item_parent ) {
			$classes[] = "{$base_class}__{$item_el}--{$child_mod}";
			$classes[] = "{$base_class_2}__{$item_el}--{$child_mod}";
		} else {
			$classes[] = "{$base_class}__{$item_el}--{$parent_mod}";
			$classes[] = "{$base_class_2}__{$item_el}--{$parent_mod}";
		}

		/**
		 * Filters the CSS class(es) applied to a menu item's list item element.
		 *
		 * @param array    $classes The CSS classes that are applied to the menu item's `<li>` element.
		 * @param WP_Post  $item    The current menu item.
		 * @param stdClass $args    An object of wp_nav_menu() arguments.
		 * @param int      $depth   Depth of menu item. Used for padding.
		 */
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) ); // WPCS: prefix ok.
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		/**
		 * Filters the ID applied to a menu item's list item element.
		 *
		 * @param string   $menu_id The ID that is applied to the menu item's `<li>` element.
		 * @param WP_Post  $item    The current menu item.
		 * @param stdClass $args    An object of wp_nav_menu() arguments.
		 * @param int      $depth   Depth of menu item. Used for padding.
		 */
		$id = apply_filters( 'nav_menu_item_id', "footer-menu-item-{$item->ID}", $item, $args, $depth ); // WPCS: prefix ok.
		$id = ! empty( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

		// Create list classes.
		$list_classes = "{$col_class} small-6 medium-3 large-2 {$footer_class}__nav-menu {$base_class} {$base_class}--footer {$base_class_2}";

		// Check if you are displaying a top-level element.
		if ( '0' === $item->menu_item_parent ) {
			$output .= "{$n}<ul class='{$list_classes}'>{$n}";
		}

		$output .= "{$indent}{$t}<li{$id}{$class_names}>";

		// phpcs:disable
		$atts = array();
		$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
		$atts['target'] = ! empty( $item->target )     ? $item->target     : '';
		$atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
		$atts['href']   = ! empty( $item->url )        ? $item->url        : '';
		// phpcs:enable

		/**
		 * Filters the HTML attributes applied to a menu item's anchor element.
		 *
		 * @param array $atts {
		 *     The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
		 *
		 *     @type string $title  Title attribute.
		 *     @type string $target Target attribute.
		 *     @type string $rel    The rel attribute.
		 *     @type string $href   The href attribute.
		 * }
		 * @param WP_Post  $item  The current menu item.
		 * @param stdClass $args  An object of wp_nav_menu() arguments.
		 * @param int      $depth Depth of menu item. Used for padding.
		 */
		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth ); // WPCS: prefix ok.

		$attributes = '';
		foreach ( $atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value       = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}

		// This filter is documented in wp-includes/post-template.php.
		$title = apply_filters( 'the_title', $item->title, $item->ID ); // WPCS: prefix ok.

		/**
		 * Filters a menu item's title.
		 *
		 * @param string   $title The menu item's title.
		 * @param WP_Post  $item  The current menu item.
		 * @param stdClass $args  An object of wp_nav_menu() arguments.
		 * @param int      $depth Depth of menu item. Used for padding.
		 */
		$title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth ); // WPCS: prefix ok.

		$item_output  = $args->before;
		$item_output .= "<a{$attributes} class='{$base_class}__{$link_el} {$base_class_2}__{$link_el}'>";
		$item_output .= $args->link_before . $title . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;

		/**
		 * Filters a menu item's starting output.
		 *
		 * The menu item's starting output only includes `$args->before`, the opening `<a>`,
		 * the menu item's title, the closing `</a>`, and `$args->after`. Currently, there is
		 * no filter for modifying the opening and closing `<li>` for a menu item.
		 *
		 * @param string   $item_output The menu item's starting HTML output.
		 * @param WP_Post  $item        Menu item data object.
		 * @param int      $depth       Depth of menu item. Used for padding.
		 * @param stdClass $args        An object of wp_nav_menu() arguments.
		 */
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args ); // WPCS: prefix ok.
	}

	/**
	 * End the new menu element.
	 *
	 * @param  string   $output HTML output.
	 * @param  WP_Post  $item   Menu item data object.
	 * @param  integer  $depth  Depth of menu item. Used for padding.
	 * @param  stdClass $args   An object of wp_nav_menu() arguments.
	 */
	public function end_el( &$output, $item, $depth = 0, $args = array() ) {
		if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
			$t = '';
			$n = '';
		} else {
			$t = "\t";
			$n = "\n";
		}

		$menu_els      = wp_get_nav_menu_items( $this->current_menu );
		$top_level_els = 0;
		foreach ( $menu_els as $el ) {
			if ( '0' === $el->menu_item_parent ) {
				$top_level_els++;
			}
		}

		$output .= "</li>{$n}";

		if ( 0 === $depth ) {
			$output .= '</ul>';
		}

	}
}
