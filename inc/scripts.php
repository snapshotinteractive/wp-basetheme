<?php
/**
 * Enqueue all script and style files.
 *
 * @package basetheme
 */

if ( ! function_exists( 'ssi_theme_scripts' ) ) :
	/**
	 * Enqueue all script and style files.
	 */
	function ssi_theme_scripts() {

		// Theme scripts (register and then localize if using for AJAX or internationlization).
		$theme_js   = '/dist/js/app.js';
		$js_version = file_exists( get_template_directory() . $theme_js ) ? filemtime( get_template_directory() . $theme_js ) : '1.0.0';
		wp_register_script( 'theme-js', get_template_directory_uri() . $theme_js, false, $js_version, true );
		wp_localize_script( 'theme-js', 'myAjax', array(
			// Define variables for use in JS.
			'ajaxUrl'  => esc_url( admin_url( 'admin-ajax.php' ) ),
			'loading'  => esc_html__( 'Loading …', 'basetheme' ),
			'noPosts'  => esc_html__( 'No older posts found', 'basetheme' ),
			'loadMore' => esc_html__( 'Load more', 'basetheme' ),
			'themeUrl' => get_template_directory_uri(),
		) );
		wp_enqueue_script( 'theme-js' );

		// Theme styles.
		$theme_css   = '/dist/css/app.css';
		$css_version = file_exists( get_template_directory() . $theme_css ) ? filemtime( get_template_directory() . $theme_css ) : '1.0.0';
		wp_enqueue_style( 'theme', get_template_directory_uri() . $theme_css, array(), $css_version, 'all' );
	}
	add_action( 'wp_enqueue_scripts', 'ssi_theme_scripts' );
endif;

if ( ! function_exists( 'ssi_login_css' ) ) :
	/**
	 * Enqueue styles for wp-login.php
	 */
	function ssi_login_css() {
		wp_enqueue_style( 'login_css', get_template_directory_uri() . '/dist/css/login.css' );
	}
	add_action( 'login_head', 'ssi_login_css' );
endif;
