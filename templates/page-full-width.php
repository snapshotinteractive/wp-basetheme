<?php
/**
 * Template Name: Full Width
 *
 * @package basetheme
 */

get_header();
?>

<section class="page-full-width__container">
	<div class="page-full-width__grid">
		<main class="page-full-width__main">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					the_content();
				endwhile;
			endif;
			?>
		</div>
	</div>
</section>

<?php get_footer(); ?>
