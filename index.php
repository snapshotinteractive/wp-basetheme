<?php
/**
 * Main theme template file.
 *
 * @package basetheme
 */

get_header(); ?>

<div class="page-index__container">
	<div class="page-index__grid">
		<main class="page-index__main">

			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					the_content();
				endwhile;
			else :
				get_template_part( 'partials/content', 'none' );
			endif;
			?>

		</main>

		<aside class="page-index__sidebar">
			<?php get_sidebar(); ?>
		</div><!-- .columns -->
	</div><!-- .row -->
</div>

<?php get_footer(); ?>
