<?php
/**
 * Single post template.
 *
 * @package basetheme
 */

get_header(); ?>

<div class="single__container">
	<div class="single__grid">
		<main class="single__main">

			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					the_content();
				endwhile;
				the_posts_pagination( [
					'mid_size'           => 1,
					'prev_text'          => _x( 'Previous', 'previous set of posts', 'basetheme' ),
					'next_text'          => _x( 'Next', 'next set of posts', 'basetheme' ),
					'screen_reader_text' => __( 'Posts navigation', 'basetheme' ),
				] );
			else :
				get_template_part( 'partials/content', 'none' );
			endif;
			?>
		</main>

		<aside class="single__sidebar">
			<?php get_sidebar(); ?>
		</aside><!-- .single__grid -->
	</div><!-- .single__container -->
</div>

<?php get_footer(); ?>
