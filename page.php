<?php
/**
 * Default page template.
 *
 * @package basetheme
 */

get_header(); ?>

<div class="page-default__container">
	<div class="page-default__grid">
		<main class="page-default__main">

			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					the_content();
				endwhile;
			else :
				get_template_part( 'partials/content', 'none' );
			endif;
			?>

		</main>

		<aside class="page-default__sidebar">
			<?php get_sidebar(); ?>
		</div><!-- .columns -->
	</div><!-- .row -->
</div>

<?php get_footer(); ?>
