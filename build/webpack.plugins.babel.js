import webpack from 'webpack';
import CleanPlugin from 'clean-webpack-plugin';
import CopyGlobsPlugin from 'copy-globs-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import FriendlyErrorsWebpackPlugin from 'friendly-errors-webpack-plugin';
import WebpackAssetsManifest from 'webpack-assets-manifest';
import replacer from './util/assetManifestsFormatter.babel';

import { rootDir, assetsFilenames, mode, publicPath } from './webpack.settings.babel';

const plugins = [
	new CleanPlugin( [ rootDir ], {
		root: rootDir,
		verbose: false,
	} ),
	new CopyGlobsPlugin( {
		pattern: 'img/**/*',
		output: `[path]${assetsFilenames}.[ext]`,
		manifest: {},
	} ),
	new ExtractTextPlugin( {
		filename: 'css/[name].css',
		allChunks: true,
	} ),
	new webpack.ProvidePlugin( {
		$: 'jquery',
		jQuery: 'jquery',
		'window.jQuery': 'jquery',
	} ),
	// new FriendlyErrorsWebpackPlugin(),
];

if ( 'production' === mode ) {
	plugins.push( new webpack.NoEmitOnErrorsPlugin() );
	plugins.push( new WebpackAssetsManifest( {
		output: 'assets.json',
		publicPath: publicPath,
		space: 2,
		replacer: replacer,
	} ) );
}

export default plugins;
