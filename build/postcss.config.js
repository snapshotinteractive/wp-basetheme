module.exports = ( { file, options, env } ) => { // eslint-disable-line
	return {
		parser: 'production' === env ? 'postcss-safe-parser' : undefined,
		plugins: {

			// Allow v4 spec CSS
			'postcss-cssnext': options.cssnext,

			// Autoprefix
			'autoprefixer': true,

			// Minifies
			// Note: Webpack will generate a warning:
			//
			//// Warning: postcss-cssnext found a duplicate plugin ('autoprefixer')
			//// in your postcss plugins. This might be inefficient. You should
			//// remove 'autoprefixer' from your postcss plugin list since it's
			//// already included by postcss-cssnext.
			//
			// You can safely ignore this warning, as autoprefixer is used in
			// cssnano to remove unneccesary prefixes; it does not add prefixes by
			// default and should not cause compatibility issues.
			cssnano: 'production' === env ? options.cssnano : false,

			// Minifies inlinve SVG declarations
			'postcss-svgo': {},
		},
	};
};
