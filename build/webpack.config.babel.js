import { mode, assets, dist, sourceMaps, publicPath } from './webpack.settings.babel';
import plugins from './webpack.plugins.babel';
import rules from './webpack.rules.babel';

const webpackConfig = {
	context: `${assets}/`,
	entry: {
		app: [ './js/app.js', './scss/app.scss' ],
		login: [ './scss/login.scss' ],
	},
	output: {
		path: dist,
		publicPath: publicPath,
		filename: 'js/[name].js',
		library: 'EntryPoint',
	},
	devtool: ( sourceMaps ? 'inline-source-map' : 'source-map' ),
	mode: mode,
	module: {
		rules: rules,
	},
	resolve: {
		modules: [ assets, 'node_modules' ],
	},
	plugins: plugins,
	externals: {
		// select2 attempts to require jquery
		// this allows it use the already defined global variable
		jquery: 'jQuery',
	},
	watch: true,
	watchOptions: {
		ignored: '/node_modules/',
	},
};

module.exports = webpackConfig;
