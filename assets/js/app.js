// Import external dependencies
import 'jquery';
import 'what-input';

// Import everything from vendor folder
import './_vendor/_foundation';

// Import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
// Import new routes below...

// Populate Router instance with DOM routes
const routes = new Router( {
	common, // All pages
	home, // Home page
	// Add new routes below...
} );

// Load Events
jQuery( document ).ready( () => routes.loadEvents() );
