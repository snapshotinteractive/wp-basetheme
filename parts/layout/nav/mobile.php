<?php
/**
 * Mobile off-canvas nav menu.
 *
 * @package basetheme
 */

?>
<nav class="off-canvas position-right" id="js-off-canvas" data-off-canvas data-auto-focus="false">
	<?php ssi_mobile_nav(); ?>
</nav>
