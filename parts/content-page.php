<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">

		<?php if ( get_the_post_thumbnail() ) : ?>

			<?php the_post_thumbnail( '', array( 'class' => 'entry-thumb' ) ); ?>

		<?php endif; ?>

	</header>
	<div class="entry-content">

		<?php the_content(); ?>

	</div>
	<footer class="entry-footer">

		<?php
		wp_link_pages( array(
			'before' => '<nav id="page-nav"><p>Pages:',
			'after'  => '</p></nav>',
		) );
		?>

	</footer>

</article>
