<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<div class="entry-content">
		<div class="error">
			<p class="bottom"><?php esc_html_e( 'The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.', 'basetheme' ); ?></p>
		</div>
		<p><?php esc_html_e( 'Please try the following:', 'basetheme' ); ?></p>
		<ul>
			<li><?php esc_html_e( 'Check your spelling', 'basetheme' ); ?></li>
			<li><?php printf( esc_html__( 'Return to the %1$shome page%2$s', 'basetheme' ), '<a href="' . esc_url( home_url() ) . '">', '</a>' ); ?></li>
			<li><?php printf( esc_html__( 'Click the %1$sback button%2$s', 'basetheme' ), '<a href="javascript:history.back()">', '</a>' ); ?></li>
		</ul>
	</div>
</article>
