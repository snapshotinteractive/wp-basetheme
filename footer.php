<?php
/**
 * Theme footer.
 *
 * @package basetheme
 */

?>

		</div><!-- .content-area -->

		<footer id="js-site-footer" class="site-footer">
		</footer>
	</div><!-- .off-canvas-content -->
</div><!-- .off-canvas-wrapper -->

<?php wp_footer(); // Please do not load scripts here. Use inc/functions-scripts.php. ?>
</body>
</html>
